package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Muthamizhan C on 12-09-2017.
 */

public class SetPassword extends Activity {

    EditText password_edt;
    Button save_btn;

    boolean validation = true;
    ProgressBar progressBar;
    String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_password);
        initVariable();
        //get the intent values
        flag = getIntent().getStringExtra("Flag");
        setOnClickListener();
    }

    private void setOnClickListener() {
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {


                    validation();
                    if (validation) {
                        setPassword();
                    }
                } else {
                    Toast.makeText(SetPassword.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setPassword() {

        Constants urlConstants = new Constants();
        String password_key = password_edt.getText().toString();

        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/SetPassword?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&Password=" + password_key + "&UserID=" + Prefs.getInt(Constants.userId, 0);

        progressBar.setVisibility(View.VISIBLE);

        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {
                    try {
                        String status = jsonObject.getString("Status");
                        if (status.equals("SUCCESS")) {
                            if (flag.equalsIgnoreCase("LoginPage")) {
                                Toast.makeText(SetPassword.this, "Password reset successfully", Toast.LENGTH_LONG).show();
                                //password reset redirect to login page
                                Intent in = new Intent(SetPassword.this, LoginPage.class);
                                startActivity(in);
                                finish();
                            } else {
                                Toast.makeText(SetPassword.this, "Registered Successfully, Please login", Toast.LENGTH_LONG).show();
                                Intent in = new Intent(SetPassword.this, LoginPage.class);
                                startActivity(in);
                                finish();
                            }

                            progressBar.setVisibility(View.GONE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SetPassword.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SetPassword.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }

            }


        }, Request.Priority.HIGH);
    }


    private boolean validation() {
        validation = true;
        if (!Validation.hasText(password_edt)) validation = false;
        return validation;
    }

    private void initVariable() {
        password_edt = (EditText) findViewById(R.id.password_edt);
        save_btn = (Button) findViewById(R.id.save_btn);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }
}
