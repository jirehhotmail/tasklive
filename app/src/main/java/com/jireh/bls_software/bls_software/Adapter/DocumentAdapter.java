package com.jireh.bls_software.bls_software.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jireh.bls_software.bls_software.Model.Document;
import com.jireh.bls_software.bls_software.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 07-09-2017.
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder> {

    Activity activity;
    DocumentAdapter.onItemClickListener listener;
    private List<Document> documentList;

    public DocumentAdapter(Activity activity, ArrayList<Document> documents, DocumentAdapter.onItemClickListener listener) {
        this.documentList = documents;
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.document, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Document doc = documentList.get(position);
        holder.message_text.setText(doc.getMESSAGE() + "\n" + doc.getTITLE() + "." + doc.getFILETYPE());
        holder.message_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(R.id.message_text, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return documentList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView message_text;

        public MyViewHolder(View view) {
            super(view);
            message_text = (TextView) view.findViewById(R.id.message_text);

        }
    }
}