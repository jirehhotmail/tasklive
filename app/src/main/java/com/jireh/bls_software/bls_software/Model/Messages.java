package com.jireh.bls_software.bls_software.Model;

/**
 * Created by Muthamizhan C on 18-09-2017.
 */

public class Messages {

    int MESSAGEID;
    String ENTITYID;
    String MESSAGE;
    int CSUSERID;
    int USERCREATED;
    String DATECREATED;
    String USERNAME;
    String MESSAGETYPE;

    public String getMESSAGETYPE() {
        return MESSAGETYPE;
    }

    public void setMESSAGETYPE(String MESSAGETYPE) {
        this.MESSAGETYPE = MESSAGETYPE;
    }

    public int getMESSAGEID() {
        return MESSAGEID;
    }

    public void setMESSAGEID(int MESSAGEID) {
        this.MESSAGEID = MESSAGEID;
    }

    public String getENTITYID() {
        return ENTITYID;
    }

    public void setENTITYID(String ENTITYID) {
        this.ENTITYID = ENTITYID;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public int getCSUSERID() {
        return CSUSERID;
    }

    public void setCSUSERID(int CSUSERID) {
        this.CSUSERID = CSUSERID;
    }

    public int getUSERCREATED() {
        return USERCREATED;
    }

    public void setUSERCREATED(int USERCREATED) {
        this.USERCREATED = USERCREATED;
    }

    public String getDATECREATED() {
        return DATECREATED;
    }

    public void setDATECREATED(String DATECREATED) {
        this.DATECREATED = DATECREATED;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }
}
