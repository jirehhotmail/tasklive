package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Muthamizhan C on 05-09-2017.
 */

public class MyAccount extends Activity {
    TextView user_name_edt, email_edt, mobile_no,company_txt;
    ProgressBar progressBar;
    ImageView back_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        initVariable();
        company_txt.setText(Prefs.getString(Constants.companyName,""));
        setOnClickListener();
        //check internet connection
        if (isNetworkAvailable()) {
            getProfileDetails();
        } else {
            Toast.makeText(MyAccount.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
        }

    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void getProfileDetails() {
        progressBar.setVisibility(View.VISIBLE);
        Constants urlConstants = new Constants();
        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/GetAccountDetails?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                Prefs.getString(Constants.companyId, "") + "&UserID=" + Prefs.getInt(Constants.userId, 0);

        Log.d(TAG, "URL::" + url);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {

                if (result) {
                    try {

                        progressBar.setVisibility(View.GONE);
                        user_name_edt.setText(jsonObject.getString("USERNAME"));
                        email_edt.setText(jsonObject.getString("EMAILID"));
                        mobile_no.setText(jsonObject.getString("MOBILE"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(MyAccount.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }


            }


        }, Request.Priority.HIGH);


    }

    private void initVariable() {
        user_name_edt = (TextView) findViewById(R.id.user_name_edt);
        email_edt = (TextView) findViewById(R.id.email_edt);
        mobile_no = (TextView) findViewById(R.id.mobile_no);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        back_img = (ImageView) findViewById(R.id.back_img);
        company_txt = (TextView) findViewById(R.id.company_txt);
    }
}
