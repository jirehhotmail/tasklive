package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by UID-01 on 7/10/2017.
 */

public class UserRegistration extends Activity {
    public static final int MY_SOCKET_TIMEOUT_MS = 50000;
    public String TAG = "UserRegistration";
    EditText name_edt, email_edt, phone_edt, company_code_edt;
    Button register_btn;
    boolean validation = true;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_page);
        intializeVarible();


        //   displayFirebaseRegId();
        setOnClickListener();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(UserRegistration.this, LoginPage.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(in);

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setOnClickListener() {
        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    //registration validation
                    registerValidation();
                    if (validation) {
                        getTheBaseUrl();



                    }

                } else {
                    Toast.makeText(UserRegistration.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getTheBaseUrl() {



            Constants urlConstants = new Constants();
            String url = urlConstants.getBaseUrl();
            Log.d(TAG, "URL::" + url);
            progressBar.setVisibility(View.VISIBLE);

            REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
                @Override
                public void OnRestResponse(boolean result, JSONObject jsonObject) {
                    if (result) {
                        try {
                            String status = jsonObject.getString("Status");
                            if (status.equals("BaseUrl")) {
                                Prefs.putString(Constants.BaseUrlShared, "http://" + jsonObject.getString("Result"));
                                registerCompany();
                            } else {

                                Toast.makeText(UserRegistration.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(UserRegistration.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                    }

                }


            }, Request.Priority.HIGH);



    }

    private void registerCompany() {
        Constants urlConstants = new Constants();
        String name = name_edt.getText().toString();
        String emailid = email_edt.getText().toString();
        String phoneno = phone_edt.getText().toString();
        final String companyCode = company_code_edt.getText().toString();

        String url =Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/Registration?Name=" + name
                + "&Email=" + emailid + "&Phone=" + phoneno + "&CompanyCode=" + companyCode;
        Log.d(TAG, "URL::" + url);
        progressBar.setVisibility(View.VISIBLE);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {
                    try {
                        String status = jsonObject.getString("Status");
                        if (status.equals("SUCCESS")) {
                            progressBar.setVisibility(View.GONE);
                            Prefs.putInt(Constants.userId, jsonObject.getInt("UserID"));
                            Prefs.putString(Constants.companyId, companyCode);
                            Intent in = new Intent(UserRegistration.this, Verification.class);
                            startActivity(in);
                            finish();

                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(UserRegistration.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(UserRegistration.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }


        }, Request.Priority.HIGH);

    }

    private boolean registerValidation() {

        validation = true;

        if (!Validation.hasText(name_edt)) validation = false;
        if (!Validation.isEmailAddress(email_edt, true)) validation = false;
        if (!(phone_edt.getText().toString().length() == 10)) {
            phone_edt.setError("enter 10 digit number");
            validation = false;
        }
        if (!Validation.hasText(company_code_edt)) validation = false;

        return validation;

    }

    private void intializeVarible() {
        name_edt = (EditText) findViewById(R.id.name_edt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        phone_edt = (EditText) findViewById(R.id.phone_edt);
        company_code_edt = (EditText) findViewById(R.id.company_code_edt);
        register_btn = (Button) findViewById(R.id.register_btn);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }
}
