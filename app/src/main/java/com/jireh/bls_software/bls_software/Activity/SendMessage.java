package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.jireh.bls_software.bls_software.Adapter.MessageAdapter;
import com.jireh.bls_software.bls_software.App.Config;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.Model.Messages;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.VolleyLog.TAG;


/**
 * Created by UID-01 on 7/10/2017.
 */

public class SendMessage extends Activity {

    EditText message_edt;
    ImageView send_img, back_img;
    TextView logout_txt, company_txt;
    boolean validation = true;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ArrayList<Messages> messages;
    MessageAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_message_page);
     /*  SendMessage.this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);*/
        intializeVarible();
        company_txt.setText(Prefs.getString(Constants.companyName, ""));

        setOnClickListener();
        //check internet connection
        //get messages from admin
        if (isNetworkAvailable()) {
            getmessagesfromServer("onCreate");
        } else {
            Toast.makeText(SendMessage.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void getmessagesfromServer(final String flag) {
        progressBar.setVisibility(View.VISIBLE);
        Constants urlConstants = new Constants();
        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/GetMessages?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                Prefs.getString(Constants.companyId, "") + "&UserID=" + Prefs.getInt(Constants.userId, 0);

        Log.d(TAG, "URL::" + url);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {

                    String status = null;
                    try {
                        status = jsonObject.getString("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (status.equals("SUCCESS")) {

                        progressBar.setVisibility(View.GONE);
                        try {
                            String resultJsonArray = jsonObject.getString("Result");
                            System.out.println("resultJsonarray" + resultJsonArray);
                            JSONArray jsonArray = new JSONArray(resultJsonArray);
                            Messages message = new Messages();
                            messages = new ArrayList<Messages>();

                            for (int idx = 0; idx < jsonArray.length(); idx++) {

                                try {
                                    Gson gson = new Gson();
                                    message = gson.fromJson(jsonArray.get(idx).toString(), Messages.class);

                                    messages.add(message);

                                    setAdapter(flag);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else {
                        progressBar.setVisibility(View.GONE);
                        try {
                            Toast.makeText(SendMessage.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SendMessage.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }


        }, Request.Priority.HIGH);
    }

    private void setAdapter(String flag) {

        linearLayoutManager = new LinearLayoutManager(SendMessage.this);
                     /*   linearLayoutManager.setStackFromEnd(true);
                       ;*/
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        // recyclerView.setHasFixedSize(true);
        // linearLayoutManager.setStackFromEnd(true);
        adapter = new MessageAdapter(SendMessage.this, messages);
        recyclerView.setAdapter(adapter);
        adapter.notifyItemInserted(messages.size() - 1);

        linearLayoutManager.scrollToPosition(messages.size() - 1);
        //
        // recyclerView.scrollToPosition(10);
        //    recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() -1);
      /*  if (!flag.equalsIgnoreCase("onCreate")) {
         //   adapter.notifyItemInserted(messages.size() - 1);
          recyclerView.scrollToPosition(messages.size() - 1);
        }
        else
        {
            recyclerView.scrollToPosition(messages.size() - 1);
        }*/

    }


    private void intializeVarible() {
        message_edt = (EditText) findViewById(R.id.edittext_chatbox);
        send_img = (ImageView) findViewById(R.id.button_chatbox_send);
        back_img = (ImageView) findViewById(R.id.back_img);
        logout_txt = (TextView) findViewById(R.id.logout_txt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        company_txt = (TextView) findViewById(R.id.company_txt);

    }

    private void setOnClickListener() {
        send_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                messageValidation();
                if (validation) {

                    //sending messages from customer to admin
                    Constants urlConstants = new Constants();
                    String message = message_edt.getText().toString().trim().replaceAll(" ", "%20");

                    String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/InsertMessages?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                            Prefs.getString(Constants.companyId, "") + "&MessageText=" + message + "&UserID=" + Prefs.getInt(Constants.userId, 0);

                    Log.d(TAG, "URL::" + url);
                    REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
                        @Override
                        public void OnRestResponse(boolean result, JSONObject jsonObject) {
                            if (result) {
                                try {
                                    String status = jsonObject.getString("Status");
                                    if (status.equals("SUCCESS")) {


                                        Toast.makeText(SendMessage.this, "Successfully sent", Toast.LENGTH_LONG).show();
                                        message_edt.setText("");
                                        getmessagesfromServer("Send");

                                    } else if (status.equals("ERROR")) {
                                        Toast.makeText(SendMessage.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SendMessage.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                            }
                        }

                    }, Request.Priority.HIGH);

                }

            }
        });
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        logout_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SendMessage.this, LoginPage.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Prefs.putInt(Constants.userId, 0);
                startActivity(in);

                //User has successfully logged in, save this information
// We need an Editor object to make preference changes.
                SharedPreferences settings = getSharedPreferences(Config.PREFS_NAME, 0); // 0 - for private mode
                SharedPreferences.Editor editor = settings.edit();

//Set "hasLoggedIn" to true
                editor.putBoolean("hasLoggedIn", false);

// Commit the edits!
                editor.commit();

            }
        });

        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter("");

                        }
                    }, 100);
                }
            }
        });

    }

    private boolean messageValidation() {
        validation = true;
        if (!Validation.hasText(message_edt)) validation = false;
        return validation;
    }


}
