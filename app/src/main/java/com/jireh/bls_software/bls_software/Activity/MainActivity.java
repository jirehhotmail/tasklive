package com.jireh.bls_software.bls_software.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bls_software.bls_software.App.Config;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.BuildConfig;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.InternalStorageContentProvider;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private static final String CAMERA_IMAGE_FOLDER = "TL_IMAGE";
    static String mCurrentPhotoPath;
    boolean doubleBackToExitPressedOnce = false;
    ImageView send_pic_img, send_doc_img, message_img, my_acoount_img;
    TextView logout_txt, company_txt, reset_pwd_txt;
    Uri file;

    private static File getOutputMediaFile() {

   /*     String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, CAMERA_IMAGE_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(file.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");*/
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = "TLIVE" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");


        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeVariables();
        company_txt.setText(Prefs.getString(Constants.companyName, ""));

        //get permission from user
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAPTURE_AUDIO_OUTPUT) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAPTURE_AUDIO_OUTPUT, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

        }
        setOnClickListener();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
   /*             Intent in = new Intent(MainActivity.this, UploadDocument.class);
                in.putExtra(Constants.CameraUri, file.toString());
                in.putExtra("Flag", "Camera");
                startActivity(in);*/


                if (resultCode == RESULT_OK) {
                    Intent in = new Intent(MainActivity.this, UploadDocument.class);
                    in.putExtra(Constants.CameraUri, mCurrentPhotoPath);
                    in.putExtra("Flag", "Camera");
                    startActivity(in);
                }

            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {

            if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED

                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED) {

                System.out.println("recoder enabled");
            } else {
                Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
            }

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED) {

                System.out.println("camera enabled");

            } else {
                Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
            if (grantResults.length > 0 && grantResults[3] == PackageManager.PERMISSION_GRANTED) {

                System.out.println("attachment enabled");

            } else {
                Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void setOnClickListener() {
        send_pic_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

  /*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                file = Uri.fromFile(getOutputMediaFile());
                intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                startActivityForResult(intent, 100);
*/
                //MainActivityPermissions7Dispatcher.startCameraWithCheck(this);
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    photoFile = getOutputMediaFile();
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        //  Uri photoURI = Uri.fromFile(getOutputMediaFile());

                        file = FileProvider.getUriForFile(MainActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                getOutputMediaFile());
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                        startActivityForResult(takePictureIntent, 100);
                    }
                }
            }
        });
        send_doc_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, SendDocument.class);
                startActivity(in);
            }
        });
        message_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, SendMessage.class);
                startActivity(in);
            }
        });
        my_acoount_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, MyAccount.class);
                startActivity(in);
            }
        });
        logout_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, LoginPage.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Prefs.putInt(Constants.userId, 0);
                startActivity(in);
                //User has successfully logged in, save this information
// We need an Editor object to make preference changes.
                SharedPreferences settings = getSharedPreferences(Config.PREFS_NAME, 0); // 0 - for private mode
                SharedPreferences.Editor editor = settings.edit();

//Set "hasLoggedIn" to true
                editor.putBoolean("hasLoggedIn", false);

// Commit the edits!
                editor.commit();
            }


        });

        reset_pwd_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    Intent in = new Intent(MainActivity.this, SetPassword.class);
                in.putExtra("Flag", "MainActivity");
                startActivity(in);*/
            }
        });
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), CAMERA_IMAGE_FOLDER);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void initializeVariables() {
        send_pic_img = (ImageView) findViewById(R.id.send_pic_img);
        send_doc_img = (ImageView) findViewById(R.id.send_doc_img);
        message_img = (ImageView) findViewById(R.id.message_img);
        my_acoount_img = (ImageView) findViewById(R.id.my_acoount_img);
        logout_txt = (TextView) findViewById(R.id.logout_txt);
        company_txt = (TextView) findViewById(R.id.company_txt);
        reset_pwd_txt = (TextView) findViewById(R.id.reset_pwd_txt);
    }
}
