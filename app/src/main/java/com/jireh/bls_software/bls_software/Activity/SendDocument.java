package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.jireh.bls_software.bls_software.Adapter.DocumentAdapter;
import com.jireh.bls_software.bls_software.App.Config;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.BuildConfig;
import com.jireh.bls_software.bls_software.Model.Document;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.RealPathUtil;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import dmax.dialog.SpotsDialog;

import static com.android.volley.VolleyLog.TAG;


/**
 * Created by UID-01 on 7/10/2017.
 */

public class SendDocument extends Activity {
    LinearLayoutManager layoutManager;
    private static final String AUDIO_RECORDER_FOLDER = "TL_AUDIO";
    private static final String DOWNLOAD_FOLDER = "TL_DOWNLOAD";
    private static final String CAMERA_IMAGE_FOLDER = "TL_IMAGE";
    RecyclerView document_recycler;
    ImageView recorder_img, camera_img, attach_file, back_img;
    TextView logout_txt, company_txt;
    MediaRecorder mediaRecorder;
    String audiopath;
    ProgressBar progressBar;
    String mCurrentPhotoPath;
    File fileStorage = null;
    File outputFile = null;
    private AlertDialog progressDialog;
    private String downloadUrl = "", downloadFileName = "";
    private Handler mHandler = new Handler();
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {

            mHandler.postDelayed(mTickExecutor, 100);
        }
    };
    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Toast.makeText(SendDocument.this, "Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
        }
    };
    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {

            Toast.makeText(SendDocument.this, "Warning: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
        }
    };

    private File getOutputMediaFile() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_document_page);
        intializeVariable();
        company_txt.setText(Prefs.getString(Constants.companyName, ""));

        setOnClickListener();


    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void getDocumentsFromServer() {
        Constants urlConstants = new Constants();
        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/GetDocuments?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                Prefs.getString(Constants.companyId, "") + "&UserID=" + Prefs.getInt(Constants.userId, 0);

        Log.d(TAG, "URL::" + url);
        progressBar.setVisibility(View.VISIBLE);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {
                    try {
                        String status = null;
                        try {
                            status = jsonObject.getString("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (status.equals("SUCCESS")) {
                            progressBar.setVisibility(View.GONE);
                            ArrayList<Document> documents = new ArrayList<Document>();
                            JSONArray documentList = jsonObject.getJSONArray("Result");
                            for (int idx = 0; idx < documentList.length(); idx++) {

                                Gson gson = new Gson();
                                Document doc = gson.fromJson(documentList.get(idx).toString(), Document.class);
                                documents.add(doc);
                            }
                            setAdapter(documents);


                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SendDocument.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SendDocument.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }
        }, Request.Priority.HIGH);
    }

    private void setAdapter(final ArrayList<Document> documents) {
        Collections.reverse(documents);
        DocumentAdapter adapter = new DocumentAdapter(SendDocument.this, documents, new DocumentAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                getDownloadDocumentList(documents.get(position).getDOCID());
            }
        });

        document_recycler.setAdapter(adapter);


    }

    private void getDownloadDocumentList(int documentId) {
        progressBar.setVisibility(View.VISIBLE);
        Constants urlConstants = new Constants();
        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/DownloadDocument?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                Prefs.getString(Constants.companyId, "") + "&UserID=" + Prefs.getInt(Constants.userId, 0) + "&DocumentID=" + documentId;

        Log.d(TAG, "URL::" + url);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {

                    String status = null;
                    try {
                        status = jsonObject.getString("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (status.equals("SUCCESS")) {
                        ArrayList<String> messages = new ArrayList<String>();
                        progressBar.setVisibility(View.GONE);
                        try {
                            String filePath = jsonObject.getString("Filepath");

                            downloadDocument(filePath);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }


                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SendDocument.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }


        }, Request.Priority.HIGH);
    }

    private void downloadDocument(String filePath) {
        URL url = null;
        try {
            url = new URL(filePath);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //    downloadFileName = FilenameUtils.getBaseName(url.getPath());
        downloadFileName = FilenameUtils.getName(url.getPath());
        downloadUrl = filePath;

        String filepath = Environment.getExternalStorageDirectory().getPath();
        fileStorage = new File(filepath, DOWNLOAD_FOLDER);
        //If File is not present create directory
        if (!fileStorage.exists()) {
            fileStorage.mkdir();
            Log.e(TAG, "Directory Created.");
        }
        System.out.println("file storage path::" + fileStorage.getAbsolutePath() + "/" + FilenameUtils.getName(url.getPath()));

        //check if the file is already downloaded or not if already downloaded open the file.
        File f = new File(fileStorage.getAbsolutePath() + "/" + FilenameUtils.getName(url.getPath().replaceAll(" ", "_")));

        if (f.exists()) {
            File file = new File(fileStorage.getAbsolutePath() + "/" + FilenameUtils.getName(url.getPath().replaceAll(" ", "_")));
/*          Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setDataAndType(Uri.fromFile(file), "**///*");
/*          intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(intent, "view"));*/
            String type = FilenameUtils.getExtension(url.getPath());

            if (type.equalsIgnoreCase("JPG") || type.equalsIgnoreCase("GIF") || type.equalsIgnoreCase("PNG") || type.equalsIgnoreCase("BMP") || type.equalsIgnoreCase("WEBP")) {
                type = "image/*";
            } else if (type.equals("MP3") ||

                    type.equalsIgnoreCase("3GP") ||
                    type.equalsIgnoreCase("MP4") ||
                    type.equalsIgnoreCase("M4A") ||
                    type.equalsIgnoreCase("AAC") ||
                    type.equalsIgnoreCase("WAV") ||
                    type.equalsIgnoreCase("TS") ||
                    type.equalsIgnoreCase("FLAC") ||
                    type.equalsIgnoreCase("OGG") ||
                    type.equalsIgnoreCase("MID") ||
                    type.equalsIgnoreCase("XMF") ||
                    type.equalsIgnoreCase("MXMF") ||
                    type.equalsIgnoreCase("RTTTL") ||
                    type.equalsIgnoreCase("RTX") ||
                    type.equalsIgnoreCase("OTA") ||
                    type.equalsIgnoreCase("IMY")) {
                type = "video/*";
            } else {
                type = "text/*";
            }

            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Intent chooser = Intent.createChooser(intent, "open with");
            intent.setDataAndType(path, type);
            startActivity(chooser);

           /* Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse(fileStorage.getAbsolutePath()+"/"+FilenameUtils.getName(url.getPath()));
            intent.setDataAndType(uri, "**///*");
           /* startActivity(Intent.createChooser(intent, "Open folder"));*/
        } else {
            //Start Downloading Task
            new DownloadingTask().execute();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        //on coming back to this activity set the visibility to recorder and gone to stop recorder
        recorder_img.setVisibility(View.VISIBLE);
        //   stop_recorder_img.setVisibility(View.GONE);
        //check internet connection
        //get all the documnents from the list
        if (isNetworkAvailable()) {
            getDocumentsFromServer();
        } else {
            Toast.makeText(SendDocument.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //if the request code for attachment get  and send the attached file path
        if (requestCode == 200) {
            String realPath = "";
            if (resultCode == Activity.RESULT_OK && data != null) {
                try {
                    realPath = RealPathUtil.getRealPath(getBaseContext(), data.getData());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent in = new Intent(SendDocument.this, UploadDocument.class);
                in.putExtra(Constants.attachmentRealPath, realPath);
                in.putExtra("Flag", "Attachment");

                // System.out.println("File path::" + FilePath);
                startActivity(in);

            }
        }

        if (requestCode == 100) {


            if (resultCode == RESULT_OK) {
                Intent in = new Intent(SendDocument.this, UploadDocument.class);
                in.putExtra(Constants.CameraUri, mCurrentPhotoPath);
                in.putExtra("Flag", "Camera");
                startActivity(in);
            }


        }
    }

    private void setOnClickListener() {
        recorder_img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //disable other two buttons during recording

                camera_img.setEnabled(false);
                attach_file.setEnabled(false);

                MediaRecorderReady();

                //show the dialog for recording
                showRecordingDialog();


            }


        });


        camera_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    photoFile = getOutputMediaFile();
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        //   Uri photoURI = Uri.fromFile(getOutputMediaFile());
                        Uri photoURI = FileProvider.getUriForFile(SendDocument.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                getOutputMediaFile());
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, 100);
                    }
                }
            }
        });
        attach_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");

                startActivityForResult(intent, 200);

            }
        });
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        logout_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SendDocument.this, LoginPage.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Prefs.putInt(Constants.userId, 0);
                startActivity(in);
                //User has successfully logged in, save this information
// We need an Editor object to make preference changes.
                SharedPreferences settings = getSharedPreferences(Config.PREFS_NAME, 0); // 0 - for private mode
                SharedPreferences.Editor editor = settings.edit();

//Set "hasLoggedIn" to true
                editor.putBoolean("hasLoggedIn", false);

// Commit the edits!
                editor.commit();
            }
        });

    }

    private void showRecordingDialog() {
        // add listener to button


        // Create custom dialog object
        final Dialog dialog = new Dialog(SendDocument.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.recording_dialog);


        ImageView image = (ImageView) dialog.findViewById(R.id.stop_recorder_img);
        dialog.setCancelable(false);

        dialog.show();

        // if decline button is clicked, close the custom dialog
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                //enable two button when recording stopped
                camera_img.setEnabled(true);
                attach_file.setEnabled(true);
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    mediaRecorder.release();
                    mediaRecorder = null;
                    mHandler.removeCallbacks(mTickExecutor);
                }
                //reset to default position and send the audio path to play the audio
                recorder_img.setVisibility(View.VISIBLE);

                Toast.makeText(SendDocument.this, "Recording stopped",
                        Toast.LENGTH_SHORT).show();

                //send the
                Intent in = new Intent(SendDocument.this, UploadDocument.class);
                in.putExtra(Constants.audioPath, audiopath);
                in.putExtra("Flag", "AudioRecorder");
                startActivity(in);

            }


        });
    }


    private void MediaRecorderReady() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        audiopath = file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".mp4";
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mediaRecorder.setAudioEncodingBitRate(48000);
        } else {
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setAudioEncodingBitRate(64000);
        }
        mediaRecorder.setAudioSamplingRate(16000);

        mediaRecorder.setOutputFile(audiopath);

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();

            mHandler.postDelayed(mTickExecutor, 100);

        } catch (IOException e) {
            Log.e("Voice Recorder", "prepare() failed " + e.getMessage());
        }
        //enable stop recorder and hide recorder when recording started

    }

    private void intializeVariable() {
        recorder_img = (ImageView) findViewById(R.id.recorder_img);
        //     stop_recorder_img = (ImageView) findViewById(R.id.stop_recorder_img);
        camera_img = (ImageView) findViewById(R.id.camera_img);
        attach_file = (ImageView) findViewById(R.id.attachment_img);
        back_img = (ImageView) findViewById(R.id.back_img);
        logout_txt = (TextView) findViewById(R.id.logout_txt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        document_recycler = (RecyclerView) findViewById(R.id.document_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(SendDocument.this);
        document_recycler.setLayoutManager(layoutManager);
        company_txt = (TextView) findViewById(R.id.company_txt);

    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  buttonText.setEnabled(false);
            progressDialog = new SpotsDialog(SendDocument.this, R.style.DownloadRecord);
            progressDialog.show();
            //   Toast.makeText(SendDocument.this, R.string.downloadStarted, Toast.LENGTH_SHORT).show();
            //  buttonText.setText();//Set Button Text when download started
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    //   buttonText.setEnabled(true);
                    progressDialog.dismiss();
                    //   buttonText.setText(R.string.downloadCompleted);//If Download completed then change button text

                    Toast.makeText(SendDocument.this, getResources().getString(R.string.downloadCompleted) + " to path ::" + fileStorage.getAbsolutePath(), Toast.LENGTH_SHORT).show();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(SendDocument.this, R.string.downloadFailed, Toast.LENGTH_SHORT).show();
                    //  buttonText.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(SendDocument.this, R.string.downloadAgain, Toast.LENGTH_SHORT).show();
                            //    buttonText.setEnabled(true);
                            //    buttonText.setText(R.string.downloadAgain);//Change button text again after 3sec
                        }
                    }, 3000);

                    Log.e(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(SendDocument.this, R.string.downloadFailed, Toast.LENGTH_SHORT).show();
                //Change button text if exception occurs
                //   buttonText.setText(R.string.downloadFailed);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(SendDocument.this, R.string.downloadAgain, Toast.LENGTH_SHORT).show();
                        //     buttonText.setEnabled(true);
                        //   buttonText.setText(R.string.downloadAgain);
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl.replaceAll(" ", "%20"));//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                outputFile = new File(fileStorage, downloadFileName.replaceAll(" ", "_"));//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }
}
