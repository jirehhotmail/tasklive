package com.jireh.bls_software.bls_software.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.commons.io.FilenameUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import dmax.dialog.SpotsDialog;

/**
 * Created by Muthamizhan C on 05-09-2017.
 */

public class UploadDocument extends Activity {
    private static final String DEBUG_TAG = "UploadDocument";
    final int PIC_CROP = 1;
    ImageView back_img, crop_img, edit_img, play_audio_img, document_img, arrowa_img, camera_rotate_img,
            send_img, pause_audio_img, pdf_img, doc_img;
    EditText message_edt;
    boolean validation = true;
    Toolbar toolbar;
    MediaPlayer mediaPlayer;
    ProgressBar progressBar;
    RelativeLayout audio_layout;
    Bitmap selectedBitmap;
    TextView messageText, attachment_txt, status_txt;
    String flag;
    RelativeLayout attachment_layout;
    private int serverResponseCode = 0;
    private AlertDialog progressDialog;
    private String upLoadServerUri = null;
    private String filepath = null, uploadPath = "";
    private SeekBar audio_seek_bar;
    private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler = new Handler();
    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();

            audio_seek_bar.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_document);
        initVariable();

        setOnClickListener();
        //get image
        try {
            flag = getIntent().getStringExtra("Flag");
            if (flag.equals("AudioRecorder")) {
                audio_layout.setVisibility(View.VISIBLE);
                document_img.setVisibility(View.GONE);

                attachment_layout.setVisibility(View.GONE);
                try {
                    mediaPlayer.setDataSource(getIntent().getStringExtra(Constants.audioPath));
                    filepath = getIntent().getStringExtra(Constants.audioPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (flag.equals("Camera")) {
                audio_layout.setVisibility(View.GONE);

                document_img.setVisibility(View.VISIBLE);

                attachment_layout.setVisibility(View.GONE);
             /*   try {
                    InputStream ims = new FileInputStream(getIntent().getStringExtra(Constants.CameraUri));
                    document_img.setImageBitmap(BitmapFactory.decodeStream(ims));
                } catch (FileNotFoundException e) {
                    return;
                }*/
                Uri imageUri = Uri.parse(getIntent().getStringExtra(Constants.CameraUri));
                File file = new File(imageUri.getPath());
                try {
                    InputStream ims = new FileInputStream(file);
                    document_img.setImageBitmap(BitmapFactory.decodeStream(ims));
                } catch (FileNotFoundException e) {
                    return;
                }


                // ScanFile so it will be appeared on Gallery
                MediaScannerConnection.scanFile(UploadDocument.this,
                        new String[]{Uri.parse(getIntent().getStringExtra(Constants.CameraUri)).getPath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                            }
                        });


                //get the camera path and save in the file path file
                Uri cameraUri = Uri.parse(getIntent().getStringExtra(Constants.CameraUri));
                filepath = cameraUri.getPath();

            } else if (flag.equals("Attachment")) {
                audio_layout.setVisibility(View.GONE);
                String extension = FilenameUtils.getExtension(getIntent().getStringExtra(Constants.attachmentRealPath));
                if (extension.equalsIgnoreCase("JPEG") ||
                        extension.equalsIgnoreCase("JPG") ||
                        extension.equalsIgnoreCase("GIF") ||
                        extension.equalsIgnoreCase("PNG") ||
                        extension.equalsIgnoreCase("BMP") ||
                        extension.equalsIgnoreCase("WebP"))

                {
                    document_img.setVisibility(View.VISIBLE);
                    attachment_layout.setVisibility(View.GONE);
                    String pathName = getIntent().getStringExtra(Constants.attachmentRealPath);
                    if (pathName != null && pathName != "") {
                        File f = new File(pathName);
                        if (f.exists()) {
                            Drawable d = Drawable.createFromPath(getIntent().getStringExtra(Constants.attachmentRealPath));
                            document_img.setImageDrawable(d);

                        }
                    }
                } else if (extension.equalsIgnoreCase("PDF")) {
                    document_img.setVisibility(View.GONE);
                    attachment_txt.setVisibility(View.VISIBLE);
                    attachment_txt.setText("File attached");
                    pdf_img.setVisibility(View.VISIBLE);
                    doc_img.setVisibility(View.GONE);
                    attachment_layout.setVisibility(View.VISIBLE);
                } else {
                    pdf_img.setVisibility(View.GONE);
                    document_img.setVisibility(View.GONE);
                    attachment_txt.setVisibility(View.VISIBLE);
                    attachment_txt.setText("File attached");
                    doc_img.setVisibility(View.VISIBLE);
                    attachment_layout.setVisibility(View.VISIBLE);
                }


                //get the attachment path and save in the file path file

                filepath = getIntent().getStringExtra(Constants.attachmentRealPath);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PIC_CROP) {
            if (data != null) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                selectedBitmap = extras.getParcelable("data");

                document_img.setImageBitmap(selectedBitmap);
            }
        } else if (resultCode != RESULT_CANCELED && requestCode == 300) {
            // get the returned data
            Bundle extras = data.getExtras();
            // get the cropped bitmap
            selectedBitmap = extras.getParcelable("data");

            document_img.setImageBitmap(selectedBitmap);
        }
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        play_audio_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(getIntent().getStringExtra(Constants.audioPath));
                    mediaPlayer.prepare();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();

                play_audio_img.setVisibility(View.GONE);
                pause_audio_img.setVisibility(View.VISIBLE);
                status_txt.setText("Click to pause the audio");


                finalTime = mediaPlayer.getDuration();
                startTime = mediaPlayer.getCurrentPosition();


                audio_seek_bar.setMax((int) finalTime);


                audio_seek_bar.setProgress((int) startTime);
                myHandler.postDelayed(UpdateSongTime, 100);

            }
        });
        pause_audio_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_audio_img.setVisibility(View.VISIBLE);
                pause_audio_img.setVisibility(View.GONE);
                mediaPlayer.pause();
                status_txt.setText("Click to play the audio");
            }
        });

        crop_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if the image is visible in this page
                if (document_img.getVisibility() == View.VISIBLE) {
                    // Its visible

                    try {
                        String path = "";
                        Intent cropIntent = new Intent("com.android.camera.action.CROP");
                        // indicate image type and Uri depends upon the selected type
                        if (flag.equals("Camera")) {
                            path = getIntent().getStringExtra(Constants.CameraUri);
                        } else if (flag.equals("Attachment")) {

                            path = getIntent().getStringExtra(Constants.attachmentRealPath);
                        }
                        cropIntent.setDataAndType(Uri.parse(path), "image/*");
                        // set crop properties here
                        cropIntent.putExtra("crop", true);
                        // indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);
                        // indicate output X and Y
                        cropIntent.putExtra("outputX", 128);
                        cropIntent.putExtra("outputY", 128);
                        // retrieve data on return
                        cropIntent.putExtra("return-data", true);
                        // start the activity - we handle returning in onActivityResult
                        startActivityForResult(cropIntent, PIC_CROP);
                    }
                    // respond to users whose devices do not support the crop action
                    catch (ActivityNotFoundException anfe) {
                        // display an error message
                        String errorMessage = "Whoops - your device doesn't support the crop action!";
                        Toast toast = Toast.makeText(UploadDocument.this, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }


            }
        });
        edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = "";
                if (flag.equals("Camera")) {
                    path = getIntent().getStringExtra(Constants.CameraUri);
                } else if (flag.equals("Attachment")) {

                    path = getIntent().getStringExtra(Constants.attachmentRealPath);
                }

                Intent editIntent = new Intent(Intent.ACTION_EDIT);
                editIntent.setDataAndType(Uri.parse(path), "image/*");
                editIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(Intent.createChooser(editIntent, null), 300);
            }
        });

        send_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check internet connection
                if (isNetworkAvailable()) {
                    uploadDocValidation();
                    if (validation) {

                        progressBar.setVisibility(View.VISIBLE);
                        Constants urlConstants = new Constants();
                        upLoadServerUri = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/PostUpload?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&CompanyID=" +
                                Prefs.getString(Constants.companyId, "") + "&Title=" + message_edt.getText().toString().trim().replaceAll(" ", "%20") + "&UserID=" + Prefs.getInt(Constants.userId, 0);


                        if (filepath != null) {
                            progressDialog = new SpotsDialog(UploadDocument.this, R.style.Custom);
                            progressDialog.show();
                            messageText.setText("uploading started.....");

                            new Thread(new Runnable() {
                                public void run() {
                                    uploadFile(filepath);
                                }
                            }).start();
                        } else {
                            Toast.makeText(UploadDocument.this, "Please try again !!!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                } else {
                    Toast.makeText(UploadDocument.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
                }
            }

        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @SuppressLint("LongLogTag")
    public int uploadFile(final String sourceFileUri) {

        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            progressDialog.dismiss();

            Log.e("uploadFile", "Source File not exist :" + filepath);

            runOnUiThread(new Runnable() {
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    messageText.setText("Source File not exist :" + filepath);
                }
            });

            return 0;

        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(
                        sourceFile);
                URL url = new URL(upLoadServerUri);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "/multipartform-data");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            String msg = "File Upload Completed";
                            messageText.setText(msg);
                            progressDialog.dismiss();
                            Toast.makeText(UploadDocument.this,
                                    "File Upload Complete.", Toast.LENGTH_SHORT)
                                    .show();
                            progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    });
                }

                // close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(UploadDocument.this,
                                "MalformedURLException", Toast.LENGTH_SHORT)
                                .show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(UploadDocument.this,
                                "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
            }
            progressDialog.dismiss();
            return serverResponseCode;
        }
    }

    private boolean uploadDocValidation() {
        validation = true;
        if (!Validation.hasText(message_edt)) validation = false;
        return validation;

    }

    private void initVariable() {
        document_img = (ImageView) findViewById(R.id.document_img);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        crop_img = (ImageView) toolbar.findViewById(R.id.crop_img);
        edit_img = (ImageView) toolbar.findViewById(R.id.edit_img);
        arrowa_img = (ImageView) findViewById(R.id.arrowa_img);
        camera_rotate_img = (ImageView) findViewById(R.id.camera_rotate_img);
        send_img = (ImageView) findViewById(R.id.send_img);
        message_edt = (EditText) findViewById(R.id.message_edt);
        play_audio_img = (ImageView) findViewById(R.id.play_audio_img);
        messageText = (TextView) findViewById(R.id.message_text);
        audio_layout = (RelativeLayout) findViewById(R.id.audio_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        attachment_txt = (TextView) findViewById(R.id.attachment_txt);
        audio_seek_bar = (SeekBar) findViewById(R.id.audio_seek_bar);
        pause_audio_img = (ImageView) findViewById(R.id.pause_audio_img);
        status_txt = (TextView) findViewById(R.id.status_txt);
        attachment_layout = (RelativeLayout) findViewById(R.id.attachment_layout);
        pdf_img = (ImageView) findViewById(R.id.pdf_img);
        doc_img = (ImageView) findViewById(R.id.doc_img);

        mediaPlayer = new MediaPlayer();
    }


    public class PhotoHandler implements Camera.PictureCallback {

        private final Context context;

        public PhotoHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFileDir = getDir();

            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

                Log.d(UploadDocument.DEBUG_TAG, "Can't create directory to save image.");
                Toast.makeText(context, "Can't create directory to save image.",
                        Toast.LENGTH_LONG).show();
                return;

            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
            String date = dateFormat.format(new Date());
            String photoFile = "Picture_" + date + ".jpg";

            String filename = pictureFileDir.getPath() + File.separator + photoFile;

            File pictureFile = new File(filename);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Toast.makeText(context, "New Image saved:" + photoFile,
                        Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                Log.d(UploadDocument.DEBUG_TAG, "File" + filename + "not saved: "
                        + error.getMessage());
                Toast.makeText(context, "Image could not be saved.",
                        Toast.LENGTH_LONG).show();
            }
        }

        private File getDir() {
            File sdDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            return new File(sdDir, "CameraAPIDemo");
        }
    }

}
