package com.jireh.bls_software.bls_software.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Muthamizhan C on 04-09-2017.
 */

public class Registration {

    @SerializedName("Status")
    String Status;
    @SerializedName("Result")
    String Result;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    @SerializedName("UserID")
    String UserID;

}
