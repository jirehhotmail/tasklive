package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.jireh.bls_software.bls_software.App.Config;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Muthamizhan C on 19-07-2017.
 */

public class LoginPage extends Activity {

    EditText email_edt, password_edt;
    Button login_btn;
    TextView no_account_txt, forget_pwd;
    boolean validation = true;

    boolean fpvalidation = true, isForgetPwdClicked = false;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences(Config.PREFS_NAME, 0);
//Get "hasLoggedIn" value. If the value doesn't exist yet false is returned
        boolean hasLoggedIn = settings.getBoolean("hasLoggedIn", false);

        if (hasLoggedIn) {
            //Go directly to main activity.
            Intent in = new Intent(LoginPage.this, MainActivity.class);
            startActivity(in);
        } else {
            setContentView(R.layout.login_layout);
            intializeVariable();
            setOnClickListener();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setOnClickListener() {
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check internet connection();
                if (isNetworkAvailable()) {

                    fieldValidation();
                    if (validation) {
                        getBaseUrl("UserLogin");

                        progressBar.setVisibility(View.VISIBLE);
                    }

                    //validate if the email id have value and get then  base url and then validate emailid
                    if (fpvalidation_proc() && isForgetPwdClicked) {
                        getBaseUrl("ForgetPwd");
                    }
                } else {
                    Toast.makeText(LoginPage.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
                }

            }
        });
        no_account_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LoginPage.this, UserRegistration.class);
                startActivity(in);
                finish();
            }
        });

        forget_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //when click forget pwd set the pwd edt to gone and login btn to validate.
                password_edt.setVisibility(View.GONE);
                forget_pwd.setVisibility(View.GONE);
                no_account_txt.setVisibility(View.GONE);
                login_btn.setText("Validate");
                isForgetPwdClicked = true;


            }
        });
    }

    private boolean fpvalidation_proc() {
        fpvalidation = true;
        if (!Validation.isEmailAddress(email_edt, true)) {
            fpvalidation = false;
        }
        return fpvalidation;
    }

    private void getBaseUrl(final String flag) {

        Constants urlConstants = new Constants();
        String url = urlConstants.getBaseUrl();
        Log.d(TAG, "URL::" + url);
        progressBar.setVisibility(View.VISIBLE);

        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {
                    try {
                        String status = jsonObject.getString("Status");
                        if (status.equals("BaseUrl")) {
                            Prefs.putString(Constants.BaseUrlShared, "http://" + jsonObject.getString("Result"));
                            if (flag.equalsIgnoreCase("UserLogin")) {
                                loginVerification();
                            } else {
                                ValidateEmailId();

                            }
                        } else {

                            Toast.makeText(LoginPage.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginPage.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }

            }


        }, Request.Priority.HIGH);


    }

    private void ValidateEmailId() {


        Constants urlConstants = new Constants();


        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/ValidateEmailID?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&Email=" + email_edt.getText().toString();
        Log.d(TAG, "URL::" + url);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {

                if (result) {
                    try {
                        String status = null;
                        try {
                            status = jsonObject.getString("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (status.equals("SUCCESS")) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginPage.this, "Verified", Toast.LENGTH_LONG);
                            //redirect to reset password page
                            Intent in = new Intent(LoginPage.this, SetPassword.class);
                            Prefs.putInt(Constants.userId, Integer.parseInt(jsonObject.getString("UserID")));
                            in.putExtra("Flag", "LoginPage");
                            startActivity(in);
                        } else {
                            Toast.makeText(LoginPage.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginPage.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }


        }, Request.Priority.HIGH);
    }

    private void loginVerification() {
        Constants urlConstants = new Constants();
        String user_name = email_edt.getText().toString();
        String password = password_edt.getText().toString();

        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/Login?Mid=" + Prefs.getString(Constants.merchantId, "015BLSCA8761") + "&UserName=" + user_name + "&Password=" + password;
        Log.d(TAG, "URL::" + url);
        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {

                if (result) {
                    try {
                        String status = null;
                        try {
                            status = jsonObject.getString("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (status.equals("SUCCESS")) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginPage.this, "Logged in Successfully", Toast.LENGTH_LONG);
                            Intent in = new Intent(LoginPage.this, MainActivity.class);
                            Prefs.putInt(Constants.userId, Integer.parseInt(jsonObject.getString("UserID")));
                            Prefs.putString(Constants.companyId, jsonObject.getString("CompanyID"));
                            Prefs.putString(Constants.companyName, jsonObject.getString("COMPANYNAME"));
                            startActivity(in);
                            finish();
                            //User has successfully logged in, save this information
// We need an Editor object to make preference changes.
                            SharedPreferences settings = getSharedPreferences(Config.PREFS_NAME, 0); // 0 - for private mode
                            SharedPreferences.Editor editor = settings.edit();

//Set "hasLoggedIn" to true
                            editor.putBoolean("hasLoggedIn", true);

// Commit the edits!
                            editor.commit();


                        } else {
                            Toast.makeText(LoginPage.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginPage.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }
            }


        }, Request.Priority.HIGH);

    }

    private boolean fieldValidation() {

        validation = true;

        if (!Validation.hasText(password_edt)) validation = false;
        if (!Validation.isEmailAddress(email_edt, true)) validation = false;


        return validation;

    }

    private void intializeVariable() {
        email_edt = (EditText) findViewById(R.id.email_edt);
        password_edt = (EditText) findViewById(R.id.password_edt);
        login_btn = (Button) findViewById(R.id.login_btn);
        no_account_txt = (TextView) findViewById(R.id.no_account_txt);
        forget_pwd = (TextView) findViewById(R.id.forget_pwd);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }
}
