package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.jireh.bls_software.bls_software.App.Constants;
import com.jireh.bls_software.bls_software.R;
import com.jireh.bls_software.bls_software.Utility.REST_API_CALL;
import com.jireh.bls_software.bls_software.Utility.Validation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by UID-01 on 7/10/2017.
 */

public class Verification extends Activity {

    public String TAG = "Verification";
    EditText license_key_edt;
    Button verify_btn;
    boolean validation = true;
    String companyId;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_page);
        intializeVariable();
        setOnClickListener();

        //get comapany id
        companyId = getIntent().getStringExtra(Constants.companyId);
    }

    private void setOnClickListener() {
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    validation();
                    if (validation) {

                        verifUser();
                    }

                } else {
                    Toast.makeText(Verification.this, "No Internet Connection, Please retry", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void verifUser() {

        Constants urlConstants = new Constants();
        String license_key = license_key_edt.getText().toString();

        String url = Prefs.getString(Constants.BaseUrlShared, "") + urlConstants.getHostname() + "/ValidateToken?Token=" + license_key + "&UserID=" + Prefs.getInt(Constants.userId, 0);
        Log.d(TAG, "URL::" + url);
        progressBar.setVisibility(View.VISIBLE);

        REST_API_CALL api = new REST_API_CALL().get(url, new REST_API_CALL.OnRestApiCallBack() {
            @Override
            public void OnRestResponse(boolean result, JSONObject jsonObject) {
                if (result) {
                    try {
                        String status = jsonObject.getString("Status");
                        if (status.equals("SUCCESS")) {
                            Toast.makeText(Verification.this, "Verified Successfully", Toast.LENGTH_LONG).show();

                            Intent in = new Intent(Verification.this, SetPassword.class);
                            in.putExtra("Flag","Verification");
                            startActivity(in);
                            finish();
                            progressBar.setVisibility(View.GONE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(Verification.this, jsonObject.getString("Result"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(Verification.this, "Technical error, please retry after some time", Toast.LENGTH_LONG).show();
                }

            }


        }, Request.Priority.HIGH);
    }

    private boolean validation() {


        validation = true;

        if (!Validation.hasText(license_key_edt)) validation = false;

        return validation;
    }

    private void intializeVariable() {
        license_key_edt = (EditText) findViewById(R.id.license_key_edt);
        verify_btn = (Button) findViewById(R.id.verify_btn);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

}
