package com.jireh.bls_software.bls_software.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bls_software.bls_software.Model.Messages;
import com.jireh.bls_software.bls_software.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MessageAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context mContext;
    private List<Messages> mMessageList;

    public MessageAdapter(Context context, List<Messages> messageList) {
        mContext = context;
        mMessageList = messageList;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        Messages message = (Messages) mMessageList.get(position);

        if (message.getMESSAGETYPE().equals("USER")) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Messages message = (Messages) mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(Messages message) {
            messageText.setText(message.getMESSAGE());
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("MMM d, h:mm a");
            Date date = null;
            try {
                date = originalFormat.parse(message.getDATECREATED().substring(0, 10) + " " + (message.getDATECREATED().substring(11, 16)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String formattedDate = targetFormat.format(date);


            // Format the stored timestamp into a readable String using method.
            timeText.setText(formattedDate);

        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            //profileImage = (ImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Messages message) {
            messageText.setText(message.getMESSAGE());
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("MMM d, h:mm a");
            Date date = null;
            try {
                date = originalFormat.parse(message.getDATECREATED().substring(0, 10) + " " + (message.getDATECREATED().substring(11, 16)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String formattedDate = targetFormat.format(date);
            // Format the stored timestamp into a readable String using method.
            timeText.setText(formattedDate);

            nameText.setText(message.getUSERNAME().substring(0,1).toUpperCase() + message.getUSERNAME().substring(1));

            // Insert the profile image from the URL into the ImageView.
           // Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);
        }
    }
}