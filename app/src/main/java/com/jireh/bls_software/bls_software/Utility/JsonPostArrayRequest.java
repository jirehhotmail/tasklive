package com.jireh.bls_software.bls_software.Utility;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class JsonPostArrayRequest extends JsonRequest<JSONObject> {
    JSONArray parms;

    public JsonPostArrayRequest(String url,  Response.Listener<JSONObject> listener, Response.ErrorListener errorListener,JSONArray params) {
        super(Method.POST, url, null, listener, errorListener);
        this.parms =params;
    }

    public byte[] getBody()
    {
        if(this.parms !=null && this.parms.length()>0)
        {
           return  encodeParameters(this.parms,getParamsEncoding());
        }
        return  null;
    }

    private byte[] encodeParameters(JSONArray parms, String paramsEncoding) {
        try {
            return parms.toString().getBytes(paramsEncoding);
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Exception not support"+paramsEncoding,uee);

        }
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return  Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Response.error(new ParseError(e));

        } catch (JSONException e) {
            Response.error(new ParseError(e));

        }
        return null;
    }
}
