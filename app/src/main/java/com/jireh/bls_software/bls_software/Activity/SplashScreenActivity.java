package com.jireh.bls_software.bls_software.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.jireh.bls_software.bls_software.R;

/**
 * Created by Muthamizhan C on 22-09-2017.
 */

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new CountDownTimer(1500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                //navigate to login page
                Intent in =new Intent(SplashScreenActivity.this,LoginPage.class);
                startActivity(in);
                finish();
            }
        }.start();

    }


}
