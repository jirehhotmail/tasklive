package com.jireh.bls_software.bls_software.Utility;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.jireh.bls_software.bls_software.App.Singleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class REST_API_CALL {

    private static final String TAG = "VOLLEY";
    private static final String tag_json_obj_req = "json_object_req";
    private static final int MY_SOCKET_TIMEOUT_MS = 5000;

    public REST_API_CALL post(String url, final String imageUrl, final OnRestApiCallBackString onRestApiCallBackString, final Request.Priority priority) {
        //sending image to server
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("true")) {
                    onRestApiCallBackString.OnRestStringResponse(true, response);
                } else {
                    onRestApiCallBackString.OnRestStringResponse(false, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                onRestApiCallBackString.OnRestStringResponse(false, null);
            }
        }) {
            public Priority getPriority() {
                return priority;
            }

            //adding parameters to send
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("image", imageUrl);
                return parameters;
            }
        };
        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(request, tag_json_obj_req);
        return null;
    }


    public void post(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void post(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void post(String url, JSONArray params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONArray jsonObjectParms = null;
        if (jsonObjectParms != null) {
            jsonObjectParms = params;
        }


        JsonPostArrayRequest jsonObjReq = new JsonPostArrayRequest(
                url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }, jsonObjectParms) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void get(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params == null) {
            jsonObjectParms = null;
        } else {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public REST_API_CALL get(String url, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                        System.out.println("resposne:: rst api call" + response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);


            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

        return null;
    }

    public REST_API_CALL get(String urlJsonArry, final OnRestApiCallBackJArray OnRestApiCallBackJArray, final Request.Priority priority) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(urlJsonArry,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        VolleyLog.d(TAG, response.toString());
                        Log.d(TAG, response.toString());
                        OnRestApiCallBackJArray.OnRestJArrayResponse(true, response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }) {
            public Request.Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonArrayRequest, tag_json_obj_req);

        return null;
    }

    public void put(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.PUT, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void put(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.PUT, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void delete(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.DELETE, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public void delete(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.DELETE, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

    public interface OnRestApiCallBack {
        public void OnRestResponse(boolean result, JSONObject jsonObject);
    }

    public interface OnRestApiCallBackJArray {
        public void OnRestJArrayResponse(boolean result, JSONArray jsonArray);
    }

    public interface OnRestApiCallBackString {
        public void OnRestStringResponse(boolean result, String response);
    }
}
