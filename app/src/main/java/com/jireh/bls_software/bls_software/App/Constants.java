package com.jireh.bls_software.bls_software.App;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Muthamizhan C on 04-09-2017.
 */

public class Constants {
    public static final String userId = "UserId";
    public static final String merchantId = "MerchantId";
    public static final String companyId = "companyId";
    public static final String companyName = "companyName";
    public static final String CameraUri = "CameraUri";
    public static final String BaseUrlShared = "BaseUrlShared";
    public static final String attachmentRealPath = "attachmentRealPath";
    public static String hostname = "/api/TaskliveCAConnect";
    public static File _file;
    public static File _dir;
    public static Bitmap bitmap;
    public static String audioPath = "AudioPath";
    public static String baseUrl = "http://ca.tasklive.com/api/TaskliveCAConnect/GetBaseUrl?Mid=015BLSCA8761";

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static void setBaseUrl(String baseUrl) {
        Constants.baseUrl = baseUrl;
    }

    public static String getHostname() {
        return hostname;
    }

    public static void setHostname(String hostname) {
        Constants.hostname = hostname;
    }


    public static File get_file() {
        return _file;
    }

    public static void set_file(File _file) {
        Constants._file = _file;
    }

    public static File get_dir() {
        return _dir;
    }

    public static void set_dir(File _dir) {
        Constants._dir = _dir;
    }

    public static Bitmap getBitmap() {
        return bitmap;
    }

    public static void setBitmap(Bitmap bitmap) {
        Constants.bitmap = bitmap;
    }
}
