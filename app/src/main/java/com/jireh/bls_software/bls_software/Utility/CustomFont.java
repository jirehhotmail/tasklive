package com.jireh.bls_software.bls_software.Utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by Muthamizhan C on 21-07-2017.
 */

@SuppressLint("AppCompatCustomView")
public class CustomFont extends TextView {


    public CustomFont(Context context) {
        super(context);
        init();
    }

    public CustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "custom_font.ttf");
        setTypeface(tf, 1);
    }


}
