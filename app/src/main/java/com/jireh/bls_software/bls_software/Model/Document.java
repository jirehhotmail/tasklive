package com.jireh.bls_software.bls_software.Model;

/**
 * Created by Muthamizhan C on 11-09-2017.
 */

public class Document {

    int DOCID;
    String TITLE;
    String FILETYPE;
    String MESSAGE;

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public int getDOCID() {
        return DOCID;
    }

    public void setDOCID(int DOCID) {
        this.DOCID = DOCID;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getFILETYPE() {
        return FILETYPE;
    }

    public void setFILETYPE(String FILETYPE) {
        this.FILETYPE = FILETYPE;
    }


}
